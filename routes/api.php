<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'email','middleware' => 'checkToken'], function () {
    Route::post('reservation','EmailController@reservation');
    Route::post('privateEvent','EmailController@privateEvent');
    Route::post('bottle','EmailController@bottle');
    Route::post('tickets','EmailController@tickets');
    Route::post('foodDelivery','EmailController@foodDelivery');
    Route::post('shopping','EmailController@shopping');
    Route::post('mailingList','EmailController@mailingList');
    Route::post('contact','EmailController@contact');
    Route::post('job','EmailController@job');
});


