<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class jobMail extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    public $jobEmailContent;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = (object) $request;
        $this->jobEmailContent = $this->request->emailContent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->request;
        $subject = $data->hospitalityPosition . " - " . $data->clientAge ." - " .$data->clientAddress;
        return $this->from( $data->venueGeneralInfo['autoResponseEmail'],$data->venueGeneralInfo['venueName']." | Job Application")
            ->subject($subject)
            ->view('emails.job');
    }
}
