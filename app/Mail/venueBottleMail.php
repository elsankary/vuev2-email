<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class venueBottleMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public $date;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->request;
        $this->date =  date('m/d/Y', strtotime($data->bottleDate));
        $subject = $this->date . " - " .$data['bottleTime']. " - $" .$data['totalPrice'] ." Paid - " . $data['noOfGuests'] ." PPL";

        return $this->from( $data->venueGeneralInfo['autoResponseEmail'],$data->venueGeneralInfo['venueName']." | Bottle Service")
            ->subject($subject)
            ->view('emails.venueOwner_Bottle');
    }
}
