<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class venuePrivateEventMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public $payment;
    public function __construct($request,$payment=false)
    {
        $this->request = (object) $request;
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->request;
        $data->eventDate = date('m/d/Y', strtotime($data->eventDate));

        $subject = $data->eventDate . " - " . $data->timeReservedFrom . " - " . $data->privateEventFinancial['guestsCount'] ." PPL - $" . $data->privateEventFinancial['clientBudget'];
        return $this->from( $data->venueGeneralInfo['autoResponseEmail'],$data->venueGeneralInfo['venueName']." | Private Event")
            ->subject($subject)
            ->view('emails.venueOwner_PrivateEvent');
    }
}
