<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class reservationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    public $reservationEmailContent;
    public $updatedFlag;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request,$updatedFlag = null)
    {
        $this->request = (object) $request;
        $this->reservationEmailContent = $this->request->emailContent;
        $this->updatedFlag = $updatedFlag;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->request;
        $data->dateReserved = date('m/d/Y', strtotime($data->dateReserved));

        if($this->updatedFlag) {
            $subject = $data->dateReserved . " - " . $data->timeReservedFrom." "." - " . $data->guestsCount ." PPL - Updated";
        } elseif(!empty($data->totalPrice)){
            $subject = $data->dateReserved . " - " . $data->timeReservedFrom." - " . $data->guestsCount ." PPL" . " - ".$data->totalPrice;
        } else {
            $subject = $data->dateReserved . " - " . $data->timeReservedFrom . " " . " - " . $data->guestsCount . " PPL";
        }

        return $this->from( $data->venueGeneralInfo['autoResponseEmail'],$data->venueGeneralInfo['venueName']." | Reservation")
            ->subject($subject)
            ->view('emails.reservation');
    }
}
