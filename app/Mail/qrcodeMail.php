<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class qrcodeMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public  $apiLink;
    public  $emailContent;
    public $attend;
    public function __construct($request,$attend)
    {
        $this->request = (object) $request;
        $this->attend = $attend;
        $this->emailContent = $this->request->emailContent;
        $this->apiLink = env('Vnu_Link');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->attend;
        $data['event_ticket']['event']['eventDate'] = date('m/d/Y', strtotime($data['event_ticket']['event']['eventDate']));
        $subject = $data['event_ticket']['event']['eventDate'] . " - " .$data['event_ticket']['event']['eventTimeFrom']. " - $" .$data['event_ticket']['ticketValue'] ." Paid";
        return $this->from( $this->request->venueGeneralInfo['autoResponseEmail'],$this->request->venueGeneralInfo['venueName']." | Tickets")
            ->subject($subject)
            ->view('emails.qrcodeMail');
    }
}
