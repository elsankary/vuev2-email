<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class contactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    public $contactEmailContent;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = (object) $request;
        $this->contactEmailContent = $this->request->emailContent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Contact";
        return $this->from( $this->request->venueGeneralInfo['autoResponseEmail'],$this->request->venueGeneralInfo['venueName']." | Contact")
            ->subject($subject)
            ->view('emails.contact');
    }
}
