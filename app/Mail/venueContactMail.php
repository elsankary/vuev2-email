<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class venueContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public function __construct($request)
    {
        $this->request = (object) $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->request;
        $subject = $data->clientName .' Contact';
        return $this->from( $this->request->venueGeneralInfo['autoResponseEmail'],$this->request->venueGeneralInfo['venueName']." | Contact")
            ->subject($subject)
            ->view('emails.venueOwner_Contact');
    }
}
