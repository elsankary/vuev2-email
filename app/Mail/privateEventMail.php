<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class privateEventMail extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    public $eventEmailContent;
    public $payment;
    public $hash;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request,$payment=false)
    {
        $this->request = (object) $request;
        $this->eventEmailContent = $this->request->emailContent;
        $this->hash = $this->request->hash;
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->request;
        $data->eventDate = date('m/d/Y', strtotime($data->eventDate));

        $from = $this->hash.'-'.$data->venueGeneralInfo['autoResponseEmail'];
        $subject = $data->eventDate . " - " . $data->timeReservedFrom . " - " . $data->privateEventFinancial['guestsCount'] ." PPL - $" . $data->privateEventFinancial['clientBudget'];
        if($this->payment){
            return $this->from( $from,$data->venueGeneralInfo['venueName']." | Private Event")
                ->subject($subject)
                ->replyTo($data->venueGeneralInfo['autoResponseEmail'])
                ->view('emails.privateEvent_payment');

        }else{

            return $this->from( $from,$data->venueGeneralInfo['venueName']." | Private Event")
                ->subject($subject)
                ->replyTo($data->venueGeneralInfo['autoResponseEmail'])
                ->view('emails.privateEvent');
        }
    }
}
