<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class venueTicketsMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public $venueRequest;
    public function __construct($request)
    {
        $this->request = (object) $request;
        $this->venueRequest = $this->request->venueRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->venueRequest;
        $data['date'] = date('m/d/Y', strtotime($data['created_at']));
        $data['time'] = date('h:i: A', strtotime($data['created_at']));
        $subject = $data['date'] . " - " .$data['time']. " - $" .$data['totlalTicketsPrice'] ." Paid";
        return $this->from( $this->request->venueGeneralInfo['autoResponseEmail'],$this->request->venueGeneralInfo['venueName']." | Tickets")
            ->subject($subject)
            ->view('emails.venueOwner_TicketBooking');
    }
}
