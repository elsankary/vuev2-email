<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class venueJobMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public $photoUrl;
    public function __construct($request)
    {
        $this->request = (object) $request;
        $this->photoUrl = env('Vnu_Link').$this->request->photo[0];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->request;
        $subject = $data->clientName .' - '. $data->hospitalityPosition .' - Age:'. $data->clientAge.' - Exp:'. $data->yearsOfExp;
        return $this->from( $data->venueGeneralInfo['autoResponseEmail'],$data->venueGeneralInfo['venueName']." | Job Application")
            ->subject($subject)
            ->view('emails.venueOwner_Job');
    }
}
