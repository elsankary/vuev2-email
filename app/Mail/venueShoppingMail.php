<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class venueShoppingMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public function __construct($request)
    {
        $this->request = (object) $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->request;
        $date = date('Y-m-d');
        $time = date('H:i');
        $subject = $date . "-" . $time . " - " . $data->totalPrice;
        return $this->from($data->venueGeneralInfo['autoResponseEmail'],$data->venueGeneralInfo['venueName']." | Shopping Order")
            ->subject($subject)
            ->view('emails.venueOwner_shoppingOrder');
    }
}
