<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class venueFoodDeliveryMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;
    public function __construct($request)
    {
        $this->request = (object) $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $subject = "Iron Bar Food Delivery";
        return $this->from( $this->request->venueGeneralInfo['autoResponseEmail'],$this->request->venueGeneralInfo['venueName']." | Food Delivery")
            ->subject($subject)
            ->view('emails.venueOwner_FoodDelivery');
    }
}
