<?php

namespace App\Http\Controllers;

use App\Mail\bottleMail;
use App\Mail\contactMail;
use App\Mail\foodDeliveryMail;
use App\Mail\jobMail;
use App\Mail\mailingListMail;
use App\Mail\privateEventMail;
use App\Mail\qrcodeMail;
use App\Mail\reservationMail;
use App\Mail\shoppingMail;
use App\Mail\venueBottleMail;
use App\Mail\venueContactMail;
use App\Mail\venueFoodDeliveryMail;
use App\Mail\venueJobMail;
use App\Mail\venuePrivateEventMail;
use App\Mail\venueReservationMail;
use App\Mail\venueShoppingMail;
use App\Mail\venueTicketsMail;
use Illuminate\Http\Request;
use Mail;

class EmailController extends Controller
{
    public function reservation(Request $request)
    {
        Mail::to($request->clientEmail)->send(new reservationMail($request));

        $venueOwnerEmails = explode(', ',$request->venueGeneralInfo['reservationEmail']);
        foreach($venueOwnerEmails as $venueOwnerEmail) {
            Mail::to($venueOwnerEmail)->send(new venueReservationMail($request));
        }
    }

    public function privateEvent(Request $request)
    {
        Mail::to($request->clientEmail)->send(new privateEventMail($request));

        $venueOwnerEmails = explode(', ',$request->venueGeneralInfo['eventPlanningEmail']);
        foreach($venueOwnerEmails as $venueOwnerEmail) {
            Mail::to($venueOwnerEmail)->send(new venuePrivateEventMail($request));
        }
    }

    public function bottle(Request $request)
    {
        Mail::to($request->clientEmail)->send(new bottleMail($request));

        $venueOwnerEmails = explode(', ',$request->venueGeneralInfo['bottleServiceEmail']);
        foreach($venueOwnerEmails as $venueOwnerEmail) {
            Mail::to($venueOwnerEmail)->send(new venueBottleMail($request));
        }
    }

    public function tickets(Request $request)
    {
        foreach ($request->attendee as $attend) {
            Mail::to($attend['clientEmail'])->send(new qrcodeMail($request,$attend));
        }

        $venueOwnerEmails = explode(', ',$request->venueGeneralInfo['ticketsEmail']);
        foreach($venueOwnerEmails as $venueOwnerEmail) {
            Mail::to($venueOwnerEmail)->send(new venueTicketsMail($request));
        }
    }

    public function foodDelivery(Request $request)
    {
        Mail::to($request->clientEmail)->send(new foodDeliveryMail($request));

        $venueOwnerEmails = explode(', ',$request->venueGeneralInfo['foodDeliveryEmail']);
        foreach($venueOwnerEmails as $venueOwnerEmail) {
            Mail::to($venueOwnerEmail)->send(new venueFoodDeliveryMail($request));
        }
    }

    public function shopping(Request $request)
    {
        Mail::to($request->clientEmail)->send(new shoppingMail($request));

        $venueOwnerEmails = explode(', ',$request->venueGeneralInfo['dailyDealsEmail']);
        foreach($venueOwnerEmails as $venueOwnerEmail) {
            Mail::to($venueOwnerEmail)->send(new venueShoppingMail($request));
        }
    }

    public function contact(Request $request)
    {
        Mail::to($request->clientEmail)->send(new contactMail($request));

        $venueOwnerEmails = explode(', ',$request->venueGeneralInfo['contactEmail']);
        foreach($venueOwnerEmails as $venueOwnerEmail) {
            Mail::to($venueOwnerEmail)->send(new venueContactMail($request));
        }
    }

    public function job(Request $request)
    {
        Mail::to($request->clientEmail)->send(new jobMail($request));

        $venueOwnerEmails = explode(', ',$request->venueGeneralInfo['jobsEmail']);
        foreach($venueOwnerEmails as $venueOwnerEmail) {
            Mail::to($venueOwnerEmail)->send(new venueJobMail($request));
        }
    }

    public function mailingList(Request $request)
    {
        Mail::to($request->clientEmail)->send(new mailingListMail($request));
    }
}
