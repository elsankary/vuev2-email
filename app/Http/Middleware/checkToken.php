<?php

namespace App\Http\Middleware;

use Closure;

class checkToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->token != env('Vnu_email_token')) {
            return response([
                'message' => 'Unauthenticated'
            ], 401);
        }
        return $next($request);
    }
}
