@extends('emails.layouts')
@section('mailbody')
    @include('emails.layouts.header')
    <br><br>
    <h2>New Contact Details</h2> <br>

    Name :<br>
    <span class="ownerMail">{{$request->clientName ?? null}}</span><br>

    Phone No. :<br>
    <span class="ownerMail">{{$request->clientPhoneNumber??null}}</span><br>

    Email Address :<br>
    <span class="ownerMail">{{$request->clientEmail}}</span><br>

    subject :<br>
    <span class="ownerMail">{{$request->subject??null}}</span><br>

    Message :<br>
    <span class="ownerMail">{{$request->moreInfo??null}}</span><br>


@stop