
@extends('emails.layouts')
@section('mailbody')
    @include('emails.layouts.header')
    <br><br>
    <h2>Reservation Details </h2>

    Full Name :  <br>
    <span class="ownerMail">{{$request->clientName}}</span><br>

    Phone No :   <br>
    <span class="ownerMail">{{$request->clientPhoneNumber}}</span><br>

    Email Address :   <br>
    <span class="ownerMail">{{$request->clientEmail}}</span><br>

    No. Of People :   <br>
    <span class="ownerMail">{{$request->guestsCount}}</span><br>

    Reservation Date :  <br>
    <span class="ownerMail">{{$request->dateReserved}}</span><br>

    Reservation Time :   <br>
    <span class="ownerMail">{{$request->timeReservedFrom}}</span><br>

    Referred by :   <br>
    <span class="ownerMail">{{$request->referredBy}}</span><br>

    Birth Date :   <br>
    <span class="ownerMail">{{$request->clientBirthDate}}</span><br>

    Bottle Service :  <br>
    <span class="ownerMail">{{$request->bottlesNumber??0}}</span><br>

    Notes :   <br>
    <span class="ownerMail">{{$request->clientRequest}}</span><br>

@stop