@extends('emails.layouts')
@section('mailbody')

    <h2>Order Details </h2>
    Full Name :  {{$request->clientName}} <br>
    Phone No :  {{$request->clientPhoneNumber??null}} <br>
    Email Address :  {{$request->clientEmail}} <br>
    totalPrice :  {{$request->totalPrice}} <br>

@stop