
@extends('emails.layouts.layout')


@section('daynamicPart')
    <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#e0e0e0" style="min-width:100%; background-color:#e0e0e0;"
           name="Layout_19">
        <tbody>
        <tr>
            <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#e0e0e0" style="background-color: #e0e0e0;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                    <tbody>
                    <tr>
                        <td height="20" style="font-size:1px; line-height:1px;"> </td>
                    </tr>
                    <tr>
                        <td valign="top" class="rnb-container-padding" bgcolor="#ffffff" style="background-color: #ffffff;" align="left">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                <tbody>
                                <tr>
                                    <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                        <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                            <tbody>
                                            <tr>
                                                <td style="font-size:14px; font-family:'Lato','Arial',Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                    <div>
                                                        <div style="line-height:24px;">
                                                                                                                <span style="font-size:18px;">
                                                                                                                    <span style="color:#000000;">
                                                                                                                        <strong>Hello
                                                                                                                        </strong>{{$request->clientName}}
                                                                                                                    </span>
                                                                                                                </span>
                                                        </div>


                                                        <div style="line-height:24px;">
                                                                                                                <span style="font-size:18px;">
                                                                                                                    <span style="color:#000000;">{!! $jobEmailContent !!}
                                                                                                                    </span>
                                                                                                                </span>
                                                        </div>


                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size:1px; line-height:1px;border-bottom:0px;"> </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

@stop

@section('content')


@stop
