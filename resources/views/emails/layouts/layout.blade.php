@include('emails.layouts.head')
<body>

    <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#e0e0e0" style="background-color:#e0e0e0;">

        <tbody>
            <tr style="display:none !important; font-size:1px; mso-hide: all;">
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td align="center" valign="top">
                    
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
                        <tbody>
                            <tr>

                                <td align="center" valign="top" bgcolor="#e0e0e0" style="background-color:#e0e0e0;">

                                    <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#e0e0e0"
                                        style="min-width:590px; background-color:#e0e0e0" name="Layout_0" id="Layout_0">
                                        <tbody>
                                            <tr>
                                                <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px; background-color: rgb(224, 224, 224);">
                                                    {{--  <table width="100%" cellpadding="0" border="0" align="center" cellspacing="0" style="background-color: rgb(219, 219, 219);">
                                                        <tbody>
                                                            <tr>
                                                                <td height="10" style="font-size:1px; line-height:1px;"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" height="20" style="font-size: 13px; color: rgb(89, 85, 85); font-weight: normal; text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                                                    <span style="color: rgb(89, 85, 85); text-decoration: none;">
                                                                        <a target="_blank" href="[MIRROR]" style="text-decoration: none; color: rgb(89, 85, 85);">View in browser</a>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="10" style="font-size:1px; line-height:1px;"> </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>  --}}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>

                                <td align="center" valign="top" bgcolor="#e0e0e0" style="background-color:#e0e0e0;">

                                    <div>
                                       
                                        @include('emails.layouts.header')
                                      
                                    </div>
                                </td>
                            </tr>
                            <tr>

                                <td align="center" valign="top" bgcolor="#e0e0e0" style="background-color:#e0e0e0;">

                                    <div>
                                      
                                        <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#e0e0e0" style="min-width:100%; background-color:#e0e0e0;"
                                            name="Layout_19">
                                            <tbody>
                                                <tr>
                                                    <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#e0e0e0" style="background-color: #e0e0e0;">
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">
                                                            {{--  Hello {{$request->clientName}}
                                                            {!! $eventEmailContent !!}  --}}
                                                            <br><br>
                                                            @yield('daynamicPart')
                                                        </table>
                                                            {{--  <tbody>
                                                                <tr>
                                                                    <td height="20" style="font-size:1px; line-height:1px;"> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" class="rnb-container-padding" bgcolor="#ffffff" style="background-color: #ffffff;" align="left">

                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                                                        <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="font-size:14px; font-family:'Lato','Arial',Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                                                                        <div>
                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:18px;">
                                                                                                                    <span style="color:#000000;">
                                                                                                                        <strong>Hello
                                                                                                                        </strong>Mahmoud
                                                                                                                    </span>
                                                                                                                </span>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:18px;">
                                                                                                                    <span style="color:#000000;"> </span>
                                                                                                                </span>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:18px;">
                                                                                                                    <span style="color:#000000;">Your
                                                                                                                        reservation
                                                                                                                        is
                                                                                                                        confirmed,
                                                                                                                    </span>
                                                                                                                </span>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:18px;">
                                                                                                                    <span style="color:#000000;">Hope
                                                                                                                        you
                                                                                                                        enjoy
                                                                                                                        your
                                                                                                                        time
                                                                                                                        with
                                                                                                                        us
                                                                                                                    </span>
                                                                                                                </span>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;"> </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:18px;">
                                                                                                                    <span style="color:#000000;">Join
                                                                                                                        us
                                                                                                                        for
                                                                                                                        Super
                                                                                                                        Bowl
                                                                                                                        2018
                                                                                                                    </span>
                                                                                                                </span>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <strong>
                                                                                                                    <span style="font-size:18px;">
                                                                                                                        <a href="http://ironbarnyc.com/tickets" style="text-decoration: underline; color: rgb(112, 72, 36);">
                                                                                                                            <span style="color:704824;">ironbarnyc.com/tickets</span>
                                                                                                                        </a>
                                                                                                                    </span>
                                                                                                                </strong>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;"> </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:18px;">
                                                                                                                    <span style="color:#000000;">Get 20%
                                                                                                                        off
                                                                                                                        when
                                                                                                                        you
                                                                                                                        buy
                                                                                                                        bottles
                                                                                                                        online
                                                                                                                    </span>
                                                                                                                </span>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <strong>
                                                                                                                    <span style="font-size:18px;">
                                                                                                                        <a href="http://ironbarnyc.com/bottles" style="text-decoration: underline; color: rgb(112, 72, 36);">
                                                                                                                            <span style="color:704824;">ironbarnyc.com/bottles</span>
                                                                                                                        </a>
                                                                                                                    </span>
                                                                                                                </strong>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;"> </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:18px;">
                                                                                                                    <span style="color:#000000;">Plan
                                                                                                                        your
                                                                                                                        private
                                                                                                                        events
                                                                                                                        with
                                                                                                                        us
                                                                                                                    </span>
                                                                                                                </span>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <strong>
                                                                                                                    <span style="font-size:18px;">
                                                                                                                        <a href="http://ironbarnyc.com/planning" style="text-decoration: underline; color: rgb(112, 72, 36);">
                                                                                                                            <span style="color:704824;">ironbarnyc.com/planning</span>
                                                                                                                        </a>
                                                                                                                    </span>
                                                                                                                </strong>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;"> </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:18px;">
                                                                                                                    <span style="color:#000000;"> </span>
                                                                                                                </span>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <strong>
                                                                                                                    <span style="font-size:17px;">
                                                                                                                        <span style="color:#000000;">Amir
                                                                                                                            F.
                                                                                                                        </span>
                                                                                                                    </span>
                                                                                                                </strong>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:17px;">
                                                                                                                    <span style="color:#000000;">SMS or
                                                                                                                        call
                                                                                                                        212.961.7507
                                                                                                                    </span>
                                                                                                                </span>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:17px;">
                                                                                                                    <span style="color:#000000;">Iron
                                                                                                                    </span>
                                                                                                                </span>
                                                                                                                <span style="font-size:16px;">
                                                                                                                    <span style="color:#000000;">
                                                                                                                        <span style="font-size:18px;">
                                                                                                                            <span style="font-size:17px;">Bar
                                                                                                                                :
                                                                                                                            </span>
                                                                                                                        </span>
                                                                                                                        <span style="font-size:17px;">
                                                                                                                            Bar. </span>
                                                                                                                        <span style="font-size:18px;">
                                                                                                                            <span style="font-size:17px;">Lounge
                                                                                                                                .
                                                                                                                            </span>
                                                                                                                        </span>
                                                                                                                    </span>
                                                                                                                </span>
                                                                                                                <span style="font-size:17px;">
                                                                                                                    <span style="color:#000000;">
                                                                                                                        Restaurant</span>
                                                                                                                </span>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:15px;">
                                                                                                                    <span style="font-size:16px;">
                                                                                                                        <span style="font-size:17px;">
                                                                                                                            <span style="color:#000000;">Ilforno
                                                                                                                                :
                                                                                                                            </span>
                                                                                                                            <span style="color:#000000;">
                                                                                                                                Italian Restaurant
                                                                                                                            </span>
                                                                                                                        </span>
                                                                                                                    </span>
                                                                                                                </span>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:15px;">
                                                                                                                    <span style="font-size:16px;">
                                                                                                                        <span style="font-size:17px;">
                                                                                                                            <span style="color:#000000;">Lybane
                                                                                                                                :
                                                                                                                            </span>
                                                                                                                            <span style="color:#000000;">
                                                                                                                                Wine Bar & Restaurant
                                                                                                                            </span>
                                                                                                                        </span>
                                                                                                                    </span>
                                                                                                                </span>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <strong>
                                                                                                                    <span style="font-size:15px;">
                                                                                                                        <span style="font-size:16px;">
                                                                                                                            <span style="font-size:17px;">
                                                                                                                                <span style="color:#000000;">
                                                                                                                                    <a href="http://www.Ironbarnyc.com" style="text-decoration: underline; color: rgb(112, 72, 36);">www.Ironbarnyc.com</a>
                                                                                                                                </span>
                                                                                                                            </span>
                                                                                                                        </span>
                                                                                                                    </span>
                                                                                                                </strong>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <strong>
                                                                                                                    <span style="font-size:15px;">
                                                                                                                        <span style="font-size:16px;">
                                                                                                                            <span style="font-size:17px;">
                                                                                                                                <span style="color:#000000;">
                                                                                                                                    <a href="http://www.IlFornonyc.com" style="text-decoration: underline; color: rgb(112, 72, 36);">www.IlFornonyc.com</a>
                                                                                                                                </span>
                                                                                                                            </span>
                                                                                                                        </span>
                                                                                                                    </span>
                                                                                                                </strong>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <strong>
                                                                                                                    <span style="font-size:15px;">
                                                                                                                        <span style="font-size:16px;">
                                                                                                                            <span style="font-size:17px;">
                                                                                                                                <span style="color:#000000;">
                                                                                                                                    <a href="http://www.Lybanenyc.com" style="text-decoration: underline; color: rgb(112, 72, 36);">www.Lybanenyc.com</a>
                                                                                                                                </span>
                                                                                                                            </span>
                                                                                                                        </span>
                                                                                                                    </span>
                                                                                                                </strong>
                                                                                                            </div>

                                                                                                            <div style="line-height:24px;">
                                                                                                                <span style="font-size:15px;">
                                                                                                                    <span style="font-size:16px;">
                                                                                                                        <span style="font-size:17px;">
                                                                                                                            <span style="color:#000000;">Address:
                                                                                                                                713
                                                                                                                                8th
                                                                                                                                Ave
                                                                                                                                &
                                                                                                                                45st,
                                                                                                                                New
                                                                                                                                York
                                                                                                                                10036
                                                                                                                            </span>
                                                                                                                        </span>
                                                                                                                    </span>
                                                                                                                </span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>

                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20" style="font-size:1px; line-height:1px;border-bottom:0px;"> </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>  --}}
                                                      
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                     

                                    </div>
                                </td>
                            </tr>
                            <tr>

                                <td align="center" valign="top" bgcolor="#e0e0e0" style="background-color:#e0e0e0;">

                                    <div>
                                       
                                        @yield('content')
                                    

                                    </div>
                                </td>
                            </tr>
                            <tr>

                                <td align="center" valign="top" bgcolor="#e0e0e0" style="background-color:#e0e0e0;">

                                   @include('emails.layouts.footer')
                                </td>
                            </tr>
                        </tbody>
                    </table>
                   
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>