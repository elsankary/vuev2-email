<table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#e0e0e0"
style="min-width:590px; background-color:#e0e0e0;" name="Layout_18" id="Layout_18">
<tbody>
    <tr>
        <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#031322" style="min-width:590px; background-color: #031322;">
            <table width="590" class="rnb-container" cellpadding="0" border="0" align="center" cellspacing="0">
                <tbody>
                    <tr>
                        <td height="30" style="font-size:1px; line-height:1px;"> </td>
                    </tr>
                    <tr>
                        <td valign="top" class="rnb-container-padding" style="font-size: 14px; font-family: Arial,Helvetica,sans-serif; color: #888888;"
                            align="center">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                <tbody>
                                    <tr>
                                        <td class="rnb-force-col rnb-social-width2" valign="top" style="mso-padding-alt: 0 20px 0 20px; padding-right: 28px; padding-left: 28px;">

                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="533" align="center" class="rnb-last-col-2">

                                                <tbody>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" border="0" cellspacing="0" class="rnb-social-align2" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="middle" class="rnb-text-center" ng-init="width=setSocialIconsBlockWidth(item)" width="533" align="center">
                                                                            <!--[if mso]>
                                <table align="center" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                <![endif]-->

                                                                            <div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                    <td align="center" valign="top">
                                    <![endif]-->
                                                                                <table align="left" style="float:left; display: inline-block; mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" cellpadding="0"
                                                                                    cellspacing="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                                    <a target="_blank" href="{{ $request->venueGeneralInfo['facebookLink'] }}">
                                                                                                        <img alt="Facebook" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme2/rnb_ico_fb.png">
                                                                                                    </a>
                                                                                                </span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <!--[if mso]>
                                    </td>
                                    <![endif]-->
                                                                            </div>
                                                                            <div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                    <td align="center" valign="top">
                                    <![endif]-->
                                                                                <table align="left" style="float:left; display: inline-block; mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" cellpadding="0"
                                                                                    cellspacing="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                                    <a target="_blank" href="{{ $request->venueGeneralInfo['twitterLink']}}">
                                                                                                        <img alt="Twitter" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme2/rnb_ico_tw.png">
                                                                                                    </a>
                                                                                                </span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <!--[if mso]>
                                    </td>
                                    <![endif]-->
                                                                            </div>
                                                                            <div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                    <td align="center" valign="top">
                                    <![endif]-->
                                                                                <table align="left" style="float:left; display: inline-block; mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" cellpadding="0"
                                                                                    cellspacing="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                                    <a target="_blank" href="{{ $request->venueGeneralInfo['instagramLink'] }}">
                                                                                                        <img alt="Instagram" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme2/rnb_ico_ig.png">
                                                                                                    </a>
                                                                                                </span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <!--[if mso]>
                                    </td>
                                    <![endif]-->
                                                                            </div>



                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="rnb-force-col" style="padding-top:15px; padding-right:20px; padding-left:20px; mso-padding-alt: 20px 0 0 20px;"
                            valign="top">

                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="264" align="center" class="rnb-col-2 social-text-spacing"
                                style="border-bottom:0;">

                                <tbody>
                                    <tr>
                                        <td valign="top">
                                            <table cellpadding="0" border="0" align="center" cellspacing="0" class="rnb-btn-col-content">
                                                <tbody>
                                                    <tr>
                                                        <td valign="middle" align="center" style="font-size:14px; font-family:Arial,Helvetica,sans-serif; color:#888888;" class="rnb-text-center">
                                                            <div>
                                                                <div style="line-height:24px;">
                                                                    <span style="color: rgb(211, 211, 211); font-size: 16px; background-color: transparent;">
                                                                            {!! $request->venueGeneralInfo['address'] !!}</span>
                                                                    </span>
                                                                </div>

                                                                <div style="line-height:24px;">
                                                                    <span style="font-size:16px;">
                                                                        <span style="color:#D3D3D3;">{!! $request->venueGeneralInfo['phone1'] !!}</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                            <td class="rnb-force-col" style="padding-top:15px; padding-right:20px; padding-left:20px; mso-padding-alt: 20px 0 0 20px;"
                                valign="top">
                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="264" align="center" class="rnb-col-2 social-text-spacing"
                                    style="border-bottom:0;">
                                    <tbody>
                                        <tr>
                                            <td valign="top">
                                                <table cellpadding="0" border="0" align="center" cellspacing="0" class="rnb-btn-col-content">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="middle" align="center" style="font-size:14px; font-family:Arial,Helvetica,sans-serif; color:#888888;" class="rnb-text-center">
                                                                <div>
                                                                    <div style="line-height:24px;">
                                                                        <span style="font-size:16px;">
                                                                            <a href="https://www.vnumngr.com/" style="color:#D3D3D3;">By Vnu Mngr</a>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="middle" align="center" style="font-size:14px; font-family:Arial,Helvetica,sans-serif; color:#888888;" class="rnb-text-center">
                                                                <div>
                                                                    <div style="line-height:24px;">
                                                                        <span style="font-size:16px;">
                                                                            <a href="https://venuelista.com/" style="color:#D3D3D3;">Join Venue Lista</a>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    <tr>
                        <td height="30" style="font-size:1px; line-height:1px;"> </td>
                    </tr>
                </tbody>
            </table>

        </td>
    </tr>
</tbody>
</table>