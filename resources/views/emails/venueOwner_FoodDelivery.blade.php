
@extends('emails.layouts')
@section('mailbody')
    @include('emails.layouts.header')
    <br><br>
    <h2>Food Delivery Details </h2>

    Full Name :  <br>
    <span class="ownerMail">{{$request->clientName}}</span><br>

    Phone No :   <br>
    <span class="ownerMail">{{$request->clientPhoneNumber}}</span><br>

    Email Address :   <br>
    <span class="ownerMail">{{$request->clientEmail}}</span><br>

    Items :  <br>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Item</th>
            <th scope="col">PRICE</th>
            <th scope="col">QUANTITY</th>
            <th scope="col">TOTAL</th>
        </tr>
        </thead>
        <tbody>
        @foreach($request->foodItems as $item)
            <tr>
                <th class="ownerMail" scope="col">{{$item['itemName']??''}}</th>
                <td class="ownerMail text-center">${{$item['itemPrice']??0}}</td>
                <td class="ownerMail text-center">{{$item['quantity']??0}}</td>
                <td class="ownerMail text-center">${{$item['total']??0}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    Items Price :  <br>
    <span class="ownerMail">${{$request->itemsPrice??0}}</span><br>
    Delivery Fee :  <br>
    <span class="ownerMail">${{$request->deliveryFee??0}}</span><br>
    Tax :  <br>
    <span class="ownerMail">${{$request->taxPrice??0}}</span><br>
    Total Paid :  <br>
    <span class="ownerMail">${{$request->totalPrice??0}}</span><br>


@stop