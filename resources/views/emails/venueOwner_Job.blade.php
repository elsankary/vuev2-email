
@extends('emails.layouts')
@section('mailbody')
    @include('emails.layouts.header')
    <br><br>
    <h2>New Job Application Details</h2> <br>

    <img border="0" width="203" hspace="0" vspace="0" alt="" class="rnb-col-1-img" src="{{$photoUrl}}" style="vertical-align: top; max-width: 203px; float: left;">
    <br>
    Name :<br>
    <span class="ownerMail">{{$request->clientName ?? null}}</span><br>

    Years Of Experience :<br>
    <span class="ownerMail">{{$request->yearsOfExp ?? null}}</span><br>

    Age :<br>
    <span class="ownerMail">{{$request->clientAge ?? null}}</span><br>

    Adress :<br>
    <span class="ownerMail">{{$request->clientAddress??null}}</span><br>

    Phone No. :<br>
    <span class="ownerMail">{{$request->clientPhoneNumber??null}}</span><br>

    Email Address :<br>
    <span class="ownerMail">{{$request->clientEmail}}</span><br>

    Hospitality Position :<br>
    <span class="ownerMail">{{$request->hospitalityPosition??null}}</span><br>

    Entertainment Position :<br>
    <span class="ownerMail">{{$request->entertainmentPosition??null}}</span><br>

    Cover Letter:<br>
    <span class="ownerMail">{{$request->coverLetter??null}}</span><br>


@stop