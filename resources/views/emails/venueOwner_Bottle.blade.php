
@extends('emails.layouts')
@section('mailbody')
    @include('emails.layouts.header')
    <br><br>
    <h2>Bottle Service Details </h2>

    Full Name :  <br>
    <span class="ownerMail">{{$request['clientName']}}</span><br>

    Phone No :   <br>
    <span class="ownerMail">{{$request['clientPhoneNumber']}}</span><br>

    Email Address :   <br>
    <span class="ownerMail">{{$request['clientEmail']}}</span><br>

    No. Of People :   <br>
    <span class="ownerMail">{{$request['noOfGuests']}}</span><br>

    Reservation Date :  <br>
    <span class="ownerMail">{!! $date !!}</span><br>

    Reservation Time :   <br>
    <span class="ownerMail">{{$request['bottleTime']}}</span><br>

    Bottle Count :  <br>
    <span class="ownerMail">{{count($request['orderItems'])??0}}</span><br>

    Items :  <br>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Item</th>
            <th scope="col">PRICE</th>
            <th scope="col">QUANTITY</th>
            <th scope="col">TOTAL</th>
        </tr>
        </thead>
        <tbody>
        @foreach($request['orderItems'] as $item)
            <tr>
                <th class="ownerMail" scope="col">{{$item['product']['name']??''}}</th>
                <td class="ownerMail text-center">${{$item['bottlePrice']??0}}</td>
                <td class="ownerMail text-center">{{$item['quantity']??0}}</td>
                <td class="ownerMail text-center">${{$item['quantity'] * $item['bottlePrice']}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    Sub Total :  <br>
    <span class="ownerMail">${{$request['subTotal']??0}}</span><br>

    Tax :  <br>
    <span class="ownerMail">${{$request['bottleTax']??0}}</span><br>

    Service :  <br>
    <span class="ownerMail">${{$request['bottleService']??0}}</span><br>

    Discount :  <br>
    <span class="ownerMail">${{$request['bottleDiscount']??0}}</span><br>

    Total Paid :  <br>
    <span class="ownerMail">${{$request['totalPrice']??0}}</span><br>

    Notes :   <br>
    <span class="ownerMail">{{$request['specialRequest']}}</span><br>

@stop