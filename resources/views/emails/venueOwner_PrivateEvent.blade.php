@extends('emails.layouts')
@section('mailbody')
    @include('emails.layouts.header')
    <br><br>
    <h2>Private Event Details</h2> <br><br>

    Name :<br>
    <span class="ownerMail">{{$request->clientName}}</span><br>

    Phone No. :<br>
    <span class="ownerMail">{{$request->clientPhoneNumber}}</span><br>

    Email Address :<br>
    <span class="ownerMail">{{$request->clientEmail}}</span><br>

    No. Of People :<br>
    <span class="ownerMail">{{$request->privateEventFinancial['guestsCount']}}</span><br>

    Event Date :<br>
    <span class="ownerMail">{{$request->eventDate}}</span><br>

    Event Time From :<br>
    <span class="ownerMail">{{$request->timeReservedFrom}}</span><br>

    Event Time To :<br>
    <span class="ownerMail">{{$request->timeReservedTo}}</span><br>

    Event Type :<br>
    <span class="ownerMail">{{$request->eventType}}</span><br>

    Budget :<br>
    <span class="ownerMail">${{$request->privateEventFinancial['clientBudget']}}</span><br>

    Note :<br>
    <span class="ownerMail">{{$request->clientRequest??null}}</span><br>

@stop