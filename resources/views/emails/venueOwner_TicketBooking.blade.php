
@extends('emails.layouts')
@section('mailbody')
    @include('emails.layouts.header')
    <br><br>
    <h2>Ticket Purchasing Details </h2>

    Full Name :  <br>
    <span class="ownerMail">{{$venueRequest['clientName']}}</span><br>

    Phone No :   <br>
    <span class="ownerMail">{{$venueRequest['clientPhoneNumber']}}</span><br>

    Email Address :   <br>
    <span class="ownerMail">{{$venueRequest['clientEmail']}}</span><br>

    No. Of Guests :   <br>
    <span class="ownerMail">{{$venueRequest['attendeeNumber']}}</span><br>

    Tickets :  <br>
    @foreach($venueRequest['booking_items'] as $ticket)
        <span class="ownerMail">{{$ticket['quantity']}} Of {{$ticket['ticketTitle']}} - Value: ${{$ticket['ticketValue']}}</span><br>
    @endforeach

    LET TAX :  <br>
    <span class="ownerMail">${{$venueRequest['ticketsLetTax']??0}}</span><br>

    STATE TAX :  <br>
    <span class="ownerMail">${{$venueRequest['ticketsStateTax']??0}}</span><br>

    SERVICE :  <br>
    <span class="ownerMail">${{$venueRequest['ticketsService']??0}}</span><br>

    Total Paid :  <br>
    <span class="ownerMail">${{$venueRequest['totlalTicketsPrice']??0}}</span><br>

    Notes :   <br>
    <span class="ownerMail">{{$venueRequest['specialRequest'] ?? null}}</span><br>

@stop