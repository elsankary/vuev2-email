@extends('emails.layouts.layout')


@section('daynamicPart')
    <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#e0e0e0"
           style="min-width:100%; background-color:#e0e0e0;"
           name="Layout_19">
        <tbody>
        <tr>
            <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#e0e0e0"
                style="background-color: #e0e0e0;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff"
                       style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                    <tbody>
                    <tr>
                        <td height="20" style="font-size:1px; line-height:1px;"> </td>
                    </tr>
                    <tr>
                        <td valign="top" class="rnb-container-padding" bgcolor="#ffffff"
                            style="background-color: #ffffff;" align="left">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                   class="rnb-columns-container">
                                <tbody>
                                <tr>
                                    <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                        <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%"
                                               align="left" class="rnb-col-1">

                                            <tbody>
                                            <tr>
                                                <td style="font-size:14px; font-family:'Lato','Arial',Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                    <div>
                                                        <div style="line-height:24px;">
                                                           <span style="font-size:18px;">
                                                               <span style="color:#000000;">
                                                                   <strong>Hello
                                                                   </strong>{{$request->clientName}}
                                                               </span>
                                                           </span>
                                                        </div>

                                                        <div style="line-height:24px;">
                                                            <span style="font-size:18px;">
                                                                <span style="color:#000000;"> 

                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div style="line-height:24px;">
                                                            {!! $emailContent !!}
                                                            {{-- <span style="font-size:18px;">
                                                                <span style="color:#000000;">
                                                                    <p><br></p>
                                                                    <p>Your Order is confirmed,</p>
                                                                    <p>Hope you enjoy our Food </p>
                                                                    <p> </p><p><br></p><p><br></p>
                                                                    <p>Amir F.</p>
                                                                    <p>SMS or call 212.961.7507</p>
                                                                    <p>Iron Bar : Bar. Lounge . Restaurant</p>
                                                                    <p>Ilforno : Italian Restaurant&nbsp;</p>
                                                                    <p>Lybane : Wine Bar &amp; Restaurant</p>
                                                                    <p><a href="http://www.ironbarnyc.com/" target="_blank" style="color: rgb(0, 136, 204);">www.Ironbarnyc.com</a></p>
                                                                    <p><a href="http://www.venuemarketer.com/manage/www.IlFornonyc.com" target="_blank" style="color: rgb(0, 136, 204);">www.IlFornonyc.com</a></p>
                                                                    <p><a href="http://www.venuemarketer.com/manage/www.Lybanenyc.com" target="_blank" style="color: rgb(0, 136, 204);">www.Lybanenyc.com</a></p>
                                                                    <p>Address: 713 8th Ave &amp; 45st, New York 10036</p>
                                                                </span>
                                                            </span> --}}
                                                        </div>


                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size:1px; line-height:1px;border-bottom:0px;"> </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
@stop

@section('content')
    <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#e0e0e0"
           style="min-width:100%; background-color:#e0e0e0;"
           name="Layout_20">
        <tbody>
        <tr>
            <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#e0e0e0"
                style="background-color: #e0e0e0;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#faf9e6"
                       style="background-color: rgb(250, 249, 230); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">
                    <tbody>
                    <tr>
                        <td height="15" style="font-size:1px; line-height:1px;"> </td>
                    </tr>
                    <tr>
                        <td valign="top" class="rnb-container-padding" bgcolor="#faf9e6"
                            style="background-color: #faf9e6;" align="left">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                   class="rnb-columns-container">
                                <tbody>
                                <tr>
                                    <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                        <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%"
                                               align="left" class="rnb-col-1">

                                            <tbody>
                                            <tr>
                                                <td style="font-size:14px; font-family:'Lato','Arial',Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                    <div style="line-height:48px;">
                                                                    <span style="color:#000000;">
                                                                        <span style="font-size:18px;">
                                                                            <span style="font-size:20px;">
                                                                                <strong>Order
                                                                                    Details
                                                                                </strong>
                                                                            </span>
                                                                        </span>
                                                                    </span>
                                                    </div>

                                                    <div style="line-height:24px;">
                                                                    <span style="font-size:18px;">
                                                                        <strong>Full Name
                                                                            :
                                                                        </strong>
                                                                    </span>
                                                        <span style="color:#000000;">
                                                                        <span style="font-size:18px;">
                                                                            <strong></strong> {{$request->clientName }}</span>
                                                                    </span>
                                                        <br>
                                                        <span style="font-size:18px;">
                                                                        <strong>Phone No.
                                                                            :
                                                                        </strong>
                                                                    </span>
                                                        <span style="color:#000000;">
                                                                        <span style="font-size:18px;">
                                                                            <strong>
                                                                            </strong> {{$request->clientPhoneNumber}}</span>
                                                                    </span>
                                                        <br>
                                                        <span style="font-size:18px;">
                                                                        <strong>Email Address
                                                                            :
                                                                        </strong>
                                                                    </span>
                                                        <span style="color:#000000;">
                                                                        <span style="font-size:18px;">
                                                                            {{$request->clientEmail}}
                                                                        </span>
                                                                    </span>
                                                        <br>
                                                        <span style="font-size:18px;">
                                                                        <strong>Items Price
                                                                            :
                                                                        </strong>
                                                        </span>
                                                        <span style="color:#000000;">
                                                                        <span style="font-size:18px;">
                                                                                ${{$request->itemsPrice}}
                                                                            </span>
                                                        </span>
                                                        <br>
                                                        @if($request->deliveryFee > 0)
                                                            <span style="font-size:18px;">
                                                                        <strong>Delivery Fee
                                                                            :
                                                                        </strong>
                                                        </span>

                                                            <span style="color:#000000;">
                                                                        <span style="font-size:18px;">
                                                                                ${{$request->deliveryFee}}
                                                                            </span>
                                                        </span>
                                                            <br>
                                                        @endif
                                                        @if($request->taxPrice)
                                                            <span style="font-size:18px;">
                                                                        <strong>Tax
                                                                            :
                                                                        </strong>
                                                        </span>
                                                            <span style="color:#000000;">
                                                                        <span style="font-size:18px;">
                                                                                ${{$request->taxPrice}}
                                                                            </span>
                                                        </span>
                                                            <br>
                                                        @endif
                                                        <span style="font-size:18px;">
                                                            <strong>Total Price
                                                                :
                                                            </strong>
                                                            </span>
                                                        <span style="color:#000000;">
                                                                            <span style="font-size:18px;">
                                                                                    ${{$request->totalPrice}}
                                                                                </span>
                                                            </span>
                                                        <br>


                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="30" style="font-size:1px; line-height:1px;border-bottom:0px;"> </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
@stop
