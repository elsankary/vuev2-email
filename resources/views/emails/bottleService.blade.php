
@include('emails.layouts.head')
<body>

<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#e0e0e0" style="background-color:#e0e0e0;">

    <tbody>
    <tr style="display:none !important; font-size:1px; mso-hide: all;">
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td align="center" valign="top">

            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
                <tbody>
                <tr>

                    <td align="center" valign="top" bgcolor="#e0e0e0" style="background-color:#e0e0e0;">

                        <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#e0e0e0"
                               style="min-width:590px; background-color:#e0e0e0" name="Layout_0" id="Layout_0">
                            <tbody>
                            <tr>
                                <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px; background-color: rgb(224, 224, 224);">
                                    <table width="100%" cellpadding="0" border="0" align="center" cellspacing="0" style="background-color: rgb(219, 219, 219);">
                                        <tbody>
                                        <tr>
                                            <td height="10" style="font-size:1px; line-height:1px;"> </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="20" style="font-size: 13px; color: rgb(89, 85, 85); font-weight: normal; text-align: center; font-family: Arial, Helvetica, sans-serif;">
                                                                    <span style="color: rgb(89, 85, 85); text-decoration: none;">
                                                                        <a target="_blank" href="[MIRROR]" style="text-decoration: none; color: rgb(89, 85, 85);">View in browser</a>
                                                                    </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="font-size:1px; line-height:1px;"> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>

                    <td align="center" valign="top" bgcolor="#e0e0e0" style="background-color:#e0e0e0;">

                        <div>

                            @include('emails.layouts.header')

                        </div>
                    </td>
                </tr>
                <tr>

                    <td align="center" valign="top" bgcolor="#dedede" style="background-color:#dedede;">

                        <div>

                            <!--[if mso]>
                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                            <![endif]-->

                            <!--[if mso]>
                            <td valign="top" width="590" style="width:590px;">
                            <![endif]-->

                            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#e0e0e0" style="min-width:100%; background-color:#e0e0e0;"
                                   name="Layout_19">
                                <tbody>
                                <tr>
                                    <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#e0e0e0" style="background-color: #e0e0e0;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                            <tbody>
                                            <tr>
                                                <td height="20" style="font-size:1px; line-height:1px;"> </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="rnb-container-padding" bgcolor="#ffffff" style="background-color: #ffffff;" align="left">

                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                        <tbody>
                                                        <tr>
                                                            <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                    <tbody>
                                                                    <tr>
                                                                        <td style="font-size:14px; font-family:'Lato','Arial',Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                                            <div>
                                                                                <div style="line-height:24px;">
                                                                                    <span style="font-size:18px;">
                                                                                        <span style="color:#000000;">
                                                                                            <strong>Hello
                                                                                            </strong>{{$request->clientName ??null}}
                                                                                        </span>
                                                                                    </span>
                                                                                </div>

                                                                                <div style="line-height:24px;">
                                                                                    <span style="font-size:18px;">
                                                                                        <span style="color:#000000;"> </span>
                                                                                    </span>
                                                                                </div>

                                                                                <div style="line-height:24px;">
                                                                                    <span style="font-size:18px;">
                                                                                        <span style="color:#000000;">{!! $bottleEmailContent !!}
                                                                                        </span>
                                                                                    </span>
                                                                                </div>


                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>

                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" style="font-size:1px; line-height:1px;border-bottom:0px;"> </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!--[if mso]>
                            </td>
                            <![endif]-->

                            <!--[if mso]>
                            </tr>
                            </table>
                            <![endif]-->
                        </div>
                    </td>
                </tr>
                <tr>

                    <td align="center" valign="top" bgcolor="#dedede" style="background-color:#dedede;">

                        <div>
                            <!--[if mso 15]>
                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                            <![endif]-->

                            <!--[if mso 15]>
                            <td valign="top" width="590" style="width:590px;">
                            <![endif]-->
                            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#dedede" style="min-width:100%; background-color:#dedede;"
                                   name="Layout_14" id="Layout_14">
                                <tbody>
                                <tr>
                                    <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#dedede" style="background-color: #dedede;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="max-width: 100%; min-width: 100%; table-layout: fixed; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;">
                                            <tbody>
                                            <tr>
                                                <td height="20" style="font-size:1px; line-height:1px;"> </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="rnb-container-padding" bgcolor="#ffffff" style="background-color: #ffffff;" align="left">

                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                        <tbody>
                                                        <tr>

                                                            <td class="rnb-force-col img-block-center" valign="top" width="180" style="padding-right: 20px;">

                                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" align="left" class="rnb-col-2-noborder-onright" width="180">


                                                                    <tbody>
                                                                    <tr>
                                                                        <td width="100%" class="img-block-center" valign="top" align="left">
                                                                            <div style="border-top:0px none #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block;">
                                                                                <div>
                                                                                    <img border="0" width="203" hspace="0" vspace="0" alt="" class="rnb-col-1-img" src="{{$apiLink . $request['orderQrcodePath']}}"
                                                                                </div>
                                                                                <div style="clear:both;"></div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td class="rnb-force-col" valign="top">

                                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="350" align="left" class="rnb-last-col-2">

                                                                    <tbody>
                                                                    <tr>
                                                                        <td class="rnb-mbl-float-none" style="font-size:14px; font-family:'Lato','Arial',Helvetica,sans-serif;color:#3c4858;float:right;width:350px; line-height: 21px;">
                                                                            <div style="line-height:32px;">
                                                                                                            <span style="color:#000000;">
                                                                                                                <span style="font-size:18px;">
                                                                                                                    <strong>Name
                                                                                                                        :
                                                                                                                    </strong>
                                                                                                                    {{$request->clientName ??null}}
                                                                                                                    <br>
                                                                                                                    <strong>Order Price
                                                                                                                        :
                                                                                                                    </strong>
                                                                                                                    ${{$request->totalPrice ??null}}
                                                                                                                    <br>

                                                                                                                    <strong>Guests
                                                                                                                        :
                                                                                                                    </strong>
                                                                                                                    {{$request->noOfGuests ??null}}
                                                                                                                    <br>

                                                                                                                    <strong>Date
                                                                                                                        /
                                                                                                                        Time
                                                                                                                        :
                                                                                                                    </strong>{!! $date !!}
                                                                                                                    {{$request->bottleTime ??null}}
                                                                                                                    <br>
                                                                                                                    <strong>Location
                                                                                                                        :
                                                                                                                    </strong>

                                                                                                                    {!! $request->venueGeneralInfo['address'] !!}</span>
                                                                                                            </span>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" style="font-size:1px; line-height:1px;"> </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!--[if mso 15]>
                            </td>
                            <![endif]-->

                            <!--[if mso 15]>
                            </tr>
                            </table>
                            <![endif]-->
                        </div>
                    </td>
                </tr>
                <tr>

                    <td align="center" valign="top" bgcolor="#e0e0e0" style="background-color:#e0e0e0;">

                        @include('emails.layouts.footer')
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if gte mso 9]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>

</body>

</html>